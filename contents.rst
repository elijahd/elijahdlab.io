tech blog
=========
Find here notes and articles from my adventures in software testing.

.. toctree::

    blog/spaghetti
    python/fstrings
    python/celery-in-openshift
