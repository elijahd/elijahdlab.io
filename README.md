![Build Status](https://gitlab.com/elijahd/elijahd.gitlab.io/badges/master/build.svg)

---

## elijahd.gitlab.io
Checkout this content in its full glory at [elijahd.gitlab.io](elijahd.gitlab.io)!

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml)

## Requirements

Install requirements for developing and serving locally within a python 3 environment and run `pip install -r dev-requirements.txt`
To just build the html, all you need is `requirements.txt`

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install the requirements as mentioned above
1. Generate an serve the documentation: `make serve`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

---

[ci]: https://about.gitlab.com/gitlab-ci/
