Experiements with f strings
===========================

Sometimes when I want to play with a feature of python but it is not quite
compiling in my head, I open up a `ipython`_ session and do an experiment where
I find out what happens in several scenarios that make the expected behavior
much easier for me to "grok" [*]_.

My team and I adhere to the `pep8`_ style and thus sometimes when I'm
formatting a long message I have to break it over several lines. I wanted to
use f-strings to interpolate some local variables, but I was not sure what
would happen if I was breaking it over several lines.

I test software, and so often when I'm raising an assertion error I want my
test that is failing to tell me as much as possible about how exactly the test
failed. **BROKEN!!!!**, while alarming, is not a lot of information to get out
of a test. I want to know why the test failed, what happened instead? What was
the context? How can this inform postive action?

That said, here are some cute experiemnts with `f-strings`_, a feature that
came in python 3.6. [*]_.


Case 1: simplest use of fstrings
--------------------------------
.. ipython::
     :okexcept:

   In [1]: cows = 'a herd of cattle'

   In [2]: cat = 'cat'

   In [3]: print(f'{cows} was found instead of a {cat}!')



Case 2: Multi-line strings can mix f-strings and regular strings
----------------------------------------------------------------
.. ipython::

   In [4]: cows = 'a herd of cattle'

   In [5]: cat = 'cat'

   In [6]: animals = ( f'{cows} can be freinds with a {cat}!'
      ...:              '\nBut not all {things in brackets} are'
      ...:             f'\ntreated like {cows} and {cat}s')

   In [7]: print(animals)


Case 3: You learn some things the hard way (escape sequences are different)
---------------------------------------------------------------------------
.. ipython::
     :okexcept:

   In [13]: a = {'i wish i could' :'dictionary'}

   In [14]: print(f'wow {a[\'i wish I could\']} escape')

   In [15]: print(f'but we can use other quotes for a {a["i wish i could"]}')
   but this is a. ok. dictionary


p.s. This is rendered with the `IPython Sphinx extension
<https://ipython.readthedocs.io/en/stable/sphinxext.html?module#ipython-sphinx-extension>`_
...which actually PARSES AND EXECUTES YOUR CODE....wild.

.. [*] I guess I really have been assimilated...
.. [*] I am greatful that I live in a world where I can at least keep up one
    minor version behind the latest release. Being trapped in the past because of 
    busted dependencies is...a reality many people live.


.. _ipython: https://ipython.org/
.. _pep8: https://www.python.org/dev/peps/pep-0008/
.. _f-strings: https://www.python.org/dev/peps/pep-0498/

