Celery, gunicorn and memory limits in Openshift
===============================================

I was working on a project this summer where we were serving celery in
openshift. We were working up to our first pilot, so there were plenty
of things that we knew were not ideal, but were not top priority for
just getting our MVP on the road.

One of these things was how we were running celery_, we had the celery
beat and the workers running in the same pod and this was not really
the set up we wanted if we were going to enable autoscaling, but seeing
as we had no users, this was not top priority to fix.

Problems were arising around memory consumption and strange errors
where it seemed like processes were getting killed from outside the
pod. We spent some time playing "whackamole" and thinking that perhaps
there was a memory leak.

Then I started experimenting a bit more to see exactly what it was that
pushed us over the memory limit and was causing these processes to
either get `BadAddress` errors or to just get killed.

While I was attempting to stress the system I noticed that more and
more workers kept getting kicked off. I thought, this is crazy, why
would we have configured it to have 20 workers on a pod with 528MB of
memory? I grepped around in the project and could not find anything.

Looking in the docs, it says `the number of workers defaults to the
number of cpus on the machine
<http://docs.celeryproject.org/en/latest/userguide/workers.html#concurrency>`_,
but what is that in a pod running in kubernetes/openshift? Does the pod
know it is only alloted 1/2 a cpu? What is half a cpu anyway?

I cracked open a python shell in one of our pods to find out:

**Ask multiprocessing how many cpus there are:**

.. code::

    >>> multiprocessing.cpu_count()
    8

Ok, well..that explains things. Looked like the pod had no "self
awareness" of its resource limitations, at least through regular means.
Just to confirm this, I also looked in `/proc/cpuinfo` and also was
curious about what `free` would tell me about available memory.

**Number of cpus listed in `/proc/cpuinfo`:**

.. code::

    sh-4.2$ cat /proc/cpuinfo | grep "processor" | wc -l
    8

**Free memory (from the pod's perspective):**

.. code::

    sh-4.2$ free -h
              total        used        free      shared  buff/cache   available
    Mem:            30G        6.4G        9.2G        281M         14G         21G
    Swap:            0B          0B          0B

Now it was clear, from within the pod we would get answers from the
node that the container was actually running on.

This made us also look into how our gunicorn_ workers were configured in our
api pod, and found the default was:

.. code::

    workers = multiprocessing.cpu_count() * 2 + 1

Which was way too many! (17!!!)

We immediately changed our invocation of celery to limit the concurrency to 2 workers:

.. code::

    celery -l info -A config worker --beat --concurrency 2

And configured `gunicorn` to only use 2 as well. We later did get to split into
different pods, with one celery beat pod and one pod per worker (this is set in
our deployment configs -- `see the MR on gitlab
<https://gitlab.com/cloudigrade/shiftigrade/merge_requests/58>`_)

.. _celery: http://www.celeryproject.org/
.. _gunicorn: https://gunicorn.org/
