Numerical methods in python: root finding.
==========================================

I studied applied math and spent as much time in classes where I got to program
as possible, because it was fun! Maybe crazy making at times, but fun! I only
regret that my combinitorics class did not give us more opprotunites to code,
because that could have made for some fun times, comparing numerical
approximations to our analytical approach to calculating odds.

Since I just discovered this sphinx module for rendering ipython code, I'm
going to mess around and see if I can get it to print some pretty graphs for
me.

Root finding is a common problem in mathematics...or rather, it is something
that we can do, therefore we make problems into root finding problems.

Below shows a program that creates a graph showing the different solutions you
will arrive at depending on your initial guess for an equation that has several
roots.

Newton Basins.
--------------
.. ipython::
     :okexcept:

   In [1]: cows = 'a herd of cattle'

   In [2]: cat = 'cat'

   In [3]: print(f'{cows} was found instead of a {cat}!')
