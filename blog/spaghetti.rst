How to Hogtie a Spaghetti Monster
=================================

`Slides for a presentation of this post <../_static/how_to_hogtie.pdf>`_.

We set out to build a CI/CD pipeline for an application composed of
microservices deployed on OpenShift and various other pieces of infrastructure
contemporaneously with development of the application, and learned a lot on the
way.

Approaching this application felt a bit like trying to wrangle a
spaghetti monster, as we began working with developers far before there
was even a login page, and no automated set up of all the dependencies.
Here's a condensed version of what we went through and where we hope to
go as the team responsible for testing the cloudigrade_ project.

0. Determine what the spaghetti monster is made of.
---------------------------------------------------

We arrived on the scene when several components of this project were still in
their infancy and not yet talking to each other.

There was not yet a login page, or canonical way to deploy a whole, working
application. To test it, we had to drive the effort to make standing up the
application in the correct way not just documented, but automated.

The ducks we had to get in a row:

   1) API (Django, proxied by nginx)
   2) Patternfly front end
   3) celery and Amazon SQS
   4) Additional SQS queues for other message delivery
   5) ECS cluster for other async tasks
   6) S3 bucket for use with CloudTrail
   7) PostgreSQL database (OpenShift or Amazon, depending)


1. Provide Spaghetti monsters on demand.
----------------------------------------

After walking through the entire set up once, and then finding that it took
even more manual work to clean up my environment between test runs, I turned to
ansible to provide a way to set up all our dependent infrastructure in amazon
and also included options to "clean" all resources or destroy them.

We moved gitlab to make use of their CI/CD, and this enabled us to start
standing up cloudigrade instances so we run the tests in our merge requests
against live environments. We wanted to minimize some duplication of effort
that was happening across our sub-project's pipelines, so we teamed up with
developers to move the deployment of each project into its own repo. Now we are
able to at a push of ...three buttons... orchestrate the feature branches of
the front end, API, and test suite to talk to eachother so we can test even
sooner and pre-merge. Then they can get torn down with another few button pushes.


2. Keep the spaghetti monsters from tangling.
---------------------------------------------

In early development we ran up against some simple namespacing problems in AWS
that complicated deploying whole working cloudigrades side by side for
development and testing caused data to bleed from one person's set up to
another.

While this was not something that would happen if every cloudigrade developer,
and moreover every merge request, had at least two AWS accounts themselves (for
the customer and for the application), so we had to invest some time into
making the deployment a bit more flexible. This little bit of investment is
what made our ability to create our automated pipelines to deploy not just
master on ever commit, but every feature we are working on.

3. Lie to the spaghetti monster.
--------------------------------

To test cloudigrade, we needed to create scenarios that we could not create
with real amazon activity. Cloudigrade only starts monitoring amazon activity
at the time of creation of a user account. How were we supposed to create tests
that ensured that last months data did not bleed into this months data (we did
find a bug like this), how were we supposed to test that the ui did the right
thing when it had 100 accounts, when we only had 4?

Because we always deploy through openshift, we have built in tooling to grab
the pods in openshift and inject mocked data. We have learned to mock the
messages from CloudTrail and place them in the s3 bucket to simulate instance
activity, but face limitations because cloudigrade is so tightly coupled to
AWS. It makes API calls about instances that it becomes aware of, and so we can
only lie to it so much in this way, testing its ability to read messages and
deal with bad messages. We could possibly look into mocking the AWS API, at
least for "user" accounts. Then we could more fully control the application
from the outside.

4. Understand the state of the beast.
-------------------------------------

Cloudigrade on the surface is very simple. There is a dashboard that shows the
instance activity of some specially tagged instances in a list of accounts,
shows you your cumulative hours for these accounts in a month, and a detail view
listing what the instances and images involved were. There are very few buttons
for the user to push. You can add an account and watch what happens. This does
not give good insight into what's going on! To understand what is happening, it
has been critical to have access to the celery and api logs, and to make that
service I mentioned running in ECS have a bit more to say than it originally
did.

We faced a lot of bugs in early development where things would just fail or
exit silently, and it took a lot of digging to figure out what was going on.
One thing that early testing allowed us to advocate for was more graceful and
informative failures and logging.

Now after every test run on automation I made a pytest fixture_ that brings down
the logs from all the pods in openshift, because otherwise often our test
failures cannot tell us but so much, as most bugs don't result in the
application stopping or throwing an error, but incorrect information appearing
or something just not happening. We have had to do some real detective work to
re-construct the chain of events that led to the broken state.

Some of our most important findings were configuration problems that only
presented themselves under load.

We also were able to find OpenShift administration problems that we were able
to raise up to the system admins of our cluster where they were not cleaning up
"docker detritus" from builds since we were doing builds with each push.

For our persistent test, stage and production servers we have set up log
consolidation and error handling to help us understand the health of these
instances and notice any patterns that may arise.

Future travails
---------------

Taking on the engineering work to allow us to mock external services would be a
huge win for QE.

A browsable API would also be a big boon for exploratory testing and
documenting the API for frontend developers and whenever other teams want to
integrate with our API.

Now with the ability to keep in better lock step with developers we, hope to
find fewer and fewer bugs in master, and find them before they ever get there.

All in all, the spaghetti monster treats us pretty good.

.. _cloudigrade: https://gitlab.com/cloudigrade
.. _fixture: https://gitlab.com/cloudigrade/integrade/merge_requests/122/diffs#1ff0bee376fb44e5e3e5d89082d68360f3e23497
